package bank;

/**
 *
 * @author rushil
 */
public class Bank {
    private String name;
    
    Bank(String name) {
        this.name = name;
    }
    
    public String getName() {
        return this.name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
}
